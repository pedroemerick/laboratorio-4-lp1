LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test
IMG_DIR = ./images

CC = g++
CPPLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

RM = rm -rf
RM_TUDO = rm -fr

PROG = estatisticas
PROG_EXTRA = extra

.PHONY: all clean debug doc doxygen gnuplot

all: $(PROG) $(PROG_EXTRA)

debug: CFLAGS += -g -O0
debug: $(PROG) $(PROG_EXTRA)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

$(PROG): $(OBJ_DIR)/main.o $(OBJ_DIR)/funcoes_arq.o $(OBJ_DIR)/calc_estatisticas.o $(OBJ_DIR)/saida_arquivos.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/funcoes_arq.h $(INC_DIR)/struct.h $(INC_DIR)/calc_estatisticas.h $(INC_DIR)/saida_arquivos.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/funcoes_arq.o: $(SRC_DIR)/funcoes_arq.cpp $(INC_DIR)/funcoes_arq.h $(INC_DIR)/struct.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/calc_estatisticas.o: $(SRC_DIR)/calc_estatisticas.cpp $(INC_DIR)/calc_estatisticas.h $(INC_DIR)/struct.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/saida_arquivos.o: $(SRC_DIR)/saida_arquivos.cpp $(INC_DIR)/saida_arquivos.h $(INC_DIR)/struct.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(PROG_EXTRA): $(OBJ_DIR)/main_extra.o $(OBJ_DIR)/funcoes_arq.o $(OBJ_DIR)/calc_estatisticas.o $(OBJ_DIR)/saida_arquivos.o $(OBJ_DIR)/funcoes_extra.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/main_extra.o: $(SRC_DIR)/main_extra.cpp $(INC_DIR)/funcoes_arq.h $(INC_DIR)/struct.h $(INC_DIR)/calc_estatisticas.h $(INC_DIR)/saida_arquivos.h $(INC_DIR)/funcoes_extra.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/funcoes_extra.o: $(SRC_DIR)/funcoes_extra.cpp $(INC_DIR)/funcoes_extra.h $(INC_DIR)/struct.h $(INC_DIR)/funcoes_arq.h
	$(CC) -c $(CPPLAGS) -o $@ $<

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

gnuplot:
	@echo "====================================================="
	@echo "Gerando gráfico histograma"
	@echo "====================================================="
	gnuplot -e "filename='totais.dat'" ./scripts/histograma.gnuplot
	@echo "*** [histograma.png gerado na pasta $(IMG_DIR)] ***"
	@echo "====================================================="
	@echo "====================================================="
	@echo "Gerando gráfico extra"
	@echo "====================================================="
	gnuplot -e "filename='extra.dat'" ./scripts/extra.gnuplot
	@echo "*** [extra.png gerado na pasta $(IMG_DIR)] ***"
	@echo "====================================================="

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR), $(OBJ_DIR) e $(IMG_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
	$(RM) $(IMG_DIR)/*
