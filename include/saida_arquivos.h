/**
* @file saida_arquivos.h
* @brief Declaracao dos prototipos das funções que geram os arquivos de saida
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 10/04/17
*/

#ifndef SAIDA_ARQUIVOS_H
#define SAIDA_ARQUIVOS_H

#include "struct.h"

#include <fstream>
using std::ofstream;

#include <iostream>
using std::cerr;
using std::endl;

/**
* @brief Função que gera o arquivo com as estatisticas de cada ano em questao
* @param anos Vetor com as estatisticas de cada ano
*/
void saida_estatisticas (Estatisticas anos[]);

/**
* @brief Função que gera o arquivo com o total de nascimentos de cada ano em questao
* @param anos Vetor com as estatisticas de cada ano
*/
void saida_totais (Estatisticas anos[]);

#endif