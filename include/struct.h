/**
* @file struct.h
* @brief Declaracao das estruturas usados no programa
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 10/04/17
*/

#ifndef STRUCT_H
#define STRUCT_H

#include <string>
using std::string;

/**
* @struct Stats stats.h
* @brief Tipo estrutura que agrega os dados de nascimento de um municipio
* @details Os dados cobrem os anos de 1994 a 2014
*/
struct Stats 
{
    string codigo;  /**< Codigo do municipio */
    string nome;    /**< Nome do municipio */
    int nascimentos [21];   /**< Numero de nascimentos em cada ano contabilizado */
};

/**
* @struct Estatisticas estatisticas.h
* @brief Tipo estrutura que agrega os dados estatisticos de um ano
*/
struct Estatisticas 
{
    int ano;    /**< Ano em questao */
    int maior;  /**< Maior numero de nascimentos do ano em questao */
    int menor;  /**< Menor numero de nascimentos do ano em questao */
    float media;    /**< Media do numero de nascimentos do ano em questao */
    float desvio_padrao;    /**< Desvio padrao do numero de nascimentos do ano em questao */
    int total;  /**< Total de nascimentos do ano em questao */
};

/**
* @struct Taxa taxa.h
* @brief Tipo estrutura que agrega a taxa de crescimento de cada municipio
* @details As taxas de crescimento cobrem os anos de 1994 a 2014
*/
struct Taxa
{
    string codigo;  /**< Codigo do municipio */
    string nome;    /**< Nome do municipio */
    float taxa_crescimento[20];     /**< Taxa de crescimento para cada ano */
};

#endif