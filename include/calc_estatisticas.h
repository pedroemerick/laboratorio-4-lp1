/**
* @file calc_estatisticas.h
* @brief Declaracao dos prototipos das funções para o cálculo das estatisticas de cada ano e taxa crescimento 2013-2014
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 10/04/17
*/

#ifndef CALC_ESTATISTICAS_H
#define CALC_ESTATISTICAS_H

#include <cmath>

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <iomanip>
using std::setprecision;

#include "struct.h"

/**
* @brief Função que procura o maior número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Maior número de nascimentos no ano em questão
*/
int maior_nascimento (Stats dados_municipios [], int num_municipios, int ano);

/**
* @brief Função que procura o menor número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Menor número de nascimentos no ano em questão
*/
int menor_nascimento (Stats dados_municipios [], int num_municipios, int ano);

/**
* @brief Função que calcula a media do número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Media do número de nascimentos no ano em questao
*/
float media_nascimento (Stats dados_municipios [], int num_municipios, int ano);

/**
* @brief Função que calcula o desvio padrao do número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @param media Media do número de nascimentos no ano em questao
* @return Desvio padrao do número de nascimentos no ano em questao
*/
float desvio_p_nascimento (Stats dados_municipios [], int num_municipios, int ano, float media);

/**
* @brief Função que calcula o total de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Total de nascimentos no ano em questao
*/
int total_nascimento (Stats dados_municipios [], int num_municipios, int ano);

/**
* @brief Procedimento que verifica a maior taxa de queda e maior taxa de crescimento em 2013-2014 e imprime para o usuario
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
*/
void taxa_crescimento (Stats dados_municipios [], int num_municipios);

#endif