/**
* @file funcoes_arq.h
* @brief Declaracao dos prototipos das funções que fazem algumas leituras em arquivo
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 11/04/17
*/

#ifndef FUNCOES_ARQ_H
#define FUNCOES_ARQ_H

#include "struct.h"

#include <string>
using std::string;

#include <fstream>
using std::ifstream;

#include <iostream>
using std::cerr;
using std::endl;

#include <cstdlib>

/**
* @brief Função que conta o numero de linhas de um determinado arquivo
* @param arq_dados Determinado arquivo para fazer a sua leitura
* @param num_municipios Quantidade de municipios existentes
*/
int conta_linhas (ifstream &arq_dados);

/**
* @brief Função que faz a leitura dos dados de cada municipio de um determinado arquivo
* @param arq_dados Determinado arquivo para fazer a sua leitura
* @param num_municipios Quantidade de municipios existentes
* @param dados_municipios Dados de todos municipios
*/
void carrega_arquivo (ifstream &arq_dados, int num_municipios, Stats dados_municipios []);

/**
* @brief Função que lê do arquivo o ano usado
* @param arq_dados Determinado arquivo para fazer a sua leitura
* @param ano Ano em questão
* @return O ano desejado
*/
int carrega_ano (ifstream &arq_dados, int ano);

#endif