/**
* @file funcoes_extra.h
* @brief Declaracao dos prototipos da função que calcula a taxa de crescimento de municipios, 
         da função que carrega os alvos e gera o arquivo de saida e da funcao que cria o script para geracao do grafico
* @author Pedro Emerick (p.emerick@live.com)
* @since 09/04/17
* @date 13/04/17
*/

#ifndef FUNCOES_EXTRA_H
#define FUNCOES_EXTRA_H

#include "struct.h"

#include "funcoes_arq.h"

#include <iostream>
using std::cerr;
using std::endl;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <string>
using std::string;

#include <iomanip>
using std::setprecision;
using std::setw;

/**
* @brief Função que calcula a taxa de crescimento de cada municipio em determinados anos
* @param dados_municipios Dados de todos municipios
* @param taxas_municipios Codigo, nome e taxa de crescimento de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param arq_dados Determinado arquivo para fazer a sua leitura
*/
void taxa_crescimento_extra (Stats dados_municipios [], Taxa taxas_municipios [], int num_municipios, ifstream &arq_dados);

/**
* @brief Funcao que faz a criação do script para geracao do grafico do arquivo extra.dat
* @param num_alvos Qauntidade de alvos definidos
* @param nomes_alvos Nome de cada alvo definido
*/
void cria_script (int num_alvos, string nomes_alvos []);

/**
* @brief Função que faz a leitura dos alvos e gera o arquivo de saida para plotagem do grafico
* @param taxas_municipios Codigo, nome e taxa de crescimento de todos municipios
* @param num_municipios Quantidade de municipios existentes
*/
void saida_alvos (Taxa taxas_municipios[], int num_municipios);

#endif