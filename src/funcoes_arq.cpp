/**
* @file funcoes_arq.cpp
* @brief Implementacao das funções que fazem algumas leituras em arquivo
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 11/04/17
*/

#include "funcoes_arq.h"

/**
* @brief Função que conta o numero de linhas de um determinado arquivo
* @param arq_dados Determinado arquivo para fazer a sua leitura
* @param num_municipios Quantidade de municipios existentes
*/
int conta_linhas (ifstream &arq_dados)
{
    string temp;    //String temporaria apenas para melhor leitura dos dados

    // Voltando para o inicio do arquivo
    arq_dados.clear();
    arq_dados.seekg(0, arq_dados.beg);

    int linhas = 0;

    while (getline (arq_dados, temp))
        linhas ++;

    return linhas;
}

/**
* @brief Função que faz a leitura dos dados de cada municipio de um determinado arquivo
* @param arq_dados Determinado arquivo para fazer a sua leitura
* @param num_municipios Quantidade de municipios existentes
* @param dados_municipios Dados de todos municipios
*/
void carrega_arquivo (ifstream &arq_dados, int num_municipios, Stats dados_municipios [])
{
    string temp;    // String temporaria apenas para melhor leitura dos dados
   
   // Voltando para o inicio do arquivo
    arq_dados.clear();
    arq_dados.seekg(0, arq_dados.beg);

    string nascimentos [21];        /**< String para armazenar temporariamente os nascimentos lidos */

    int soma = 0;

    getline (arq_dados, temp);

    for (int ii = 0; ii < num_municipios; ii++)
    {
        getline (arq_dados, temp, '"');
        getline (arq_dados, dados_municipios[ii].codigo, ' ');
        getline (arq_dados, dados_municipios[ii].nome, '"');
        getline (arq_dados, temp, ';');

        soma = 0;

        for (int jj = 0; jj < 22; jj++)
        {   
            if (jj != 21)
            {
                getline (arq_dados, nascimentos [jj], ';');

                if (nascimentos[jj] == "-")
                    nascimentos[jj] = "0";
                
                dados_municipios[ii].nascimentos [jj] = atoi (nascimentos [jj].c_str ());
            
                soma += dados_municipios[ii].nascimentos [jj];
            }
            else
                getline (arq_dados, temp);
            
            if (jj == 21 && atoi (temp.c_str ()) != soma)
            {
                cerr << "Erro na leitura do arquivo !!!" << endl;
                exit (1);
            }
        }
    }
}

/**
* @brief Função que lê do arquivo o ano usado
* @param arq_dados Determinado arquivo para fazer a sua leitura
* @param ano Ano em questão
* @return O ano desejado
*/
int carrega_ano (ifstream &arq_dados, int ano)
{
    // Voltando para o inicio do arquivo
    arq_dados.clear();
    arq_dados.seekg(0, arq_dados.beg);

    string ano_s;       /**< String temporaria para leitura do ano */
    string temp;
    int ano_i;      /**< Inteiro para armazenar o ano lido */

    getline (arq_dados, temp, ';');

    for (int ii = 0; ii <= ano; ii++)
    {
        getline (arq_dados, temp, '"');
        getline (arq_dados, ano_s, '"');
        getline (arq_dados, temp, ';');
    }

    ano_i = atoi (ano_s.c_str ());

    return ano_i;
}