/**
* @file calc_estatisticas.cpp
* @brief Implementacao das funções para o cálculo das estatisticas de cada ano e taxa crescimento 2013-2014
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 10/04/17
*/

#include "calc_estatisticas.h"

/**
* @brief Função que procura o maior número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Maior número de nascimentos no ano em questão
*/
int maior_nascimento (Stats dados_municipios [], int num_municipios, int ano)
{  
    int maior = dados_municipios[0].nascimentos [ano];

    for (int ii = 1; ii < num_municipios; ii++)
    {
        if (maior < dados_municipios[ii].nascimentos [ano])
            maior = dados_municipios[ii].nascimentos [ano];
    }

    return maior;
}


/**
* @brief Função que procura o menor número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Menor número de nascimentos no ano em questão
*/
int menor_nascimento (Stats dados_municipios [], int num_municipios, int ano)
{  
    int menor = dados_municipios[0].nascimentos [ano];

    for (int ii = 1; ii < num_municipios; ii++)
    {
        if (menor > dados_municipios[ii].nascimentos [ano])
            menor = dados_municipios[ii].nascimentos [ano];
    }

    return menor;
}

/**
* @brief Função que calcula a media do número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Media do número de nascimentos no ano em questao
*/
float media_nascimento (Stats dados_municipios [], int num_municipios, int ano)
{  
    float media = 0;

    for (int ii = 0; ii < num_municipios; ii++)
    {
        media += dados_municipios[ii].nascimentos[ano];
    }

    media /= num_municipios;

    return media;
}

/**
* @brief Função que calcula o desvio padrao do número de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @param media Media do número de nascimentos no ano em questao
* @return Desvio padrao do número de nascimentos no ano em questao
*/
float desvio_p_nascimento (Stats dados_municipios [], int num_municipios, int ano, float media)
{  
    float desvio_padrao;

    int somatorio = 0;

    float temp = (float) 1 / (float) num_municipios;
    
    for (int ii = 0; ii < num_municipios; ii++)
    {
        somatorio += pow ((dados_municipios[ii].nascimentos[ano] - media), 2);
    }

    desvio_padrao = (float) sqrt (temp * somatorio);

    return desvio_padrao;
}

/**
* @brief Função que calcula o total de nascimentos em um determinado ano
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param ano Ano em questão
* @return Total de nascimentos no ano em questao
*/
int total_nascimento (Stats dados_municipios [], int num_municipios, int ano)
{
    int total = 0;

    for (int ii = 0; ii < num_municipios; ii++)
    {
        total += dados_municipios[ii].nascimentos[ano];
    }

    return total;
}

/**
* @brief Procedimento que verifica a maior taxa de queda e maior taxa de crescimento em 2013-2014 e imprime para o usuario
* @param dados_municipios Dados de todos municipios
* @param num_municipios Quantidade de municipios existentes
*/
void taxa_crescimento (Stats dados_municipios [], int num_municipios)
{
    float taxa_crescimento = (float) (dados_municipios[0].nascimentos[20] / (float) dados_municipios[0].nascimentos[19]);
    float taxa_queda = (float) (dados_municipios[0].nascimentos[20] / (float) dados_municipios[0].nascimentos[19]); 

    float temp;
    int temp_ii_c = 0;      /**< Indice da maior taxa de crescimento */
    int temp_ii_q = 0;      /**< Indice da menor taxa de queda */

    for (int ii = 1; ii < num_municipios; ii++)
    {
        if (dados_municipios[ii].nascimentos[19] != 0)
            temp = (float) dados_municipios[ii].nascimentos[20] / (float) dados_municipios[ii].nascimentos[19];
        
        if (taxa_crescimento < temp)
        {
            temp_ii_c = ii;
            taxa_crescimento = temp;
        }
        if (taxa_queda > temp)
        {
            temp_ii_q = ii;
            taxa_queda = temp;
        }
    }
    
    cout << "Municipio com maior taxa de queda 2013-2014: ";
    cout << dados_municipios[temp_ii_q].nome << " (" << std::fixed << setprecision(2) << 100 * (taxa_queda - 1) << "%)" << endl;

    cout << "Municipio com maior taxa de crescimento 2013-2014: ";
    cout << dados_municipios[temp_ii_c].nome << " (+" << std::fixed << setprecision(2) << 100 * (taxa_crescimento - 1) << "%)" << endl;
}