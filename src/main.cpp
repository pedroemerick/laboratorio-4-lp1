/**
* @file main.cpp
* @brief Programa que carrega os dados de determinados municipios atraves de um arquivo e calcula algumas estatisticas
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 11/04/17
*/

#include <iostream>
using std::cerr;
using std::endl;

#include <fstream>
using std::ifstream;

#include "funcoes_arq.h"
#include "struct.h"
#include "calc_estatisticas.h"
#include "saida_arquivos.h"

/** 
 * @brief Função principal
 * @param argc Possui o número de argumentos com os quais a funçãoo main() foi chamada na linha de comando
 * @param argv Cada string desta matriz é um dos parâmetros da linha de comando
 */
int main (int argc, char* argv[])
{
    ifstream arq_dados (argv[1]);

    if (!arq_dados)
    {
        cerr << "O arquivo nao foi aberto !!!" << endl;

        return 1;
    }

    Stats *dados_municipios;

    int num_municipios = 0;

    num_municipios = conta_linhas (arq_dados);
    num_municipios -= 2;

    dados_municipios = new Stats [num_municipios];

    carrega_arquivo (arq_dados, num_municipios, dados_municipios);

    Estatisticas anos [21];

    for (int ii = 0; ii < 21; ii++)
    {   
        anos[ii].ano = carrega_ano (arq_dados, ii);
        anos[ii].maior = maior_nascimento (dados_municipios, num_municipios, ii);
        anos[ii].menor = menor_nascimento (dados_municipios, num_municipios, ii);
        anos[ii].media = media_nascimento (dados_municipios, num_municipios, ii);
        anos[ii].desvio_padrao = desvio_p_nascimento (dados_municipios, num_municipios, ii, anos[ii].media);
        anos[ii].total = total_nascimento (dados_municipios, num_municipios, ii);
    }

    saida_estatisticas (anos);
    saida_totais (anos);

    taxa_crescimento (dados_municipios, num_municipios);

    arq_dados.close ();

    delete [] dados_municipios;

    return 0;
}