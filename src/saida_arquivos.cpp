/**
* @file saida_arquivos.cpp
* @brief Implementacao das funções que geram os arquivos de saida
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 10/04/17
*/

#include "saida_arquivos.h"

/**
* @brief Função que gera o arquivo com as estatisticas de cada ano em questao
* @param anos Vetor com as estatisticas de cada ano
*/
void saida_estatisticas (Estatisticas anos[])
{
    ofstream arq_estatisticas ("./data/estatisticas.csv");

    if (!arq_estatisticas)
    {
        cerr << "O arquivo nao foi aberto !!!" << endl;

        exit (1);
    }

    for (int ii = 0; ii < 21; ii++)
    {
        arq_estatisticas << anos[ii].ano << ";";
        arq_estatisticas << anos[ii].maior << ";";
        arq_estatisticas << anos[ii].menor << ";";
        arq_estatisticas << anos[ii].media << ";";
        arq_estatisticas << anos[ii].desvio_padrao << ";";
        arq_estatisticas << anos[ii].total << endl;
    }

    cerr << "... Arquivo estatisticas.csv gerado" << endl;

    arq_estatisticas.close ();
}

/**
* @brief Função que gera o arquivo com o total de nascimentos de cada ano em questao
* @param anos Vetor com as estatisticas de cada ano
*/
void saida_totais (Estatisticas anos[])
{
    ofstream arq_totais ("./data/totais.dat");

    if (!arq_totais)
    {
        cerr << "O arquivo nao foi aberto !!!" << endl;

        exit (1);
    }

    for (int ii = 0; ii < 21; ii++)
    {
        arq_totais << anos[ii].ano << " ";
        arq_totais << anos[ii].total << endl;
    }

    cerr << "... Arquivo totais.dat gerado" << endl << endl;

    arq_totais.close ();
}
