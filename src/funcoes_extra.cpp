/**
* @file funcoes_extra.cpp
* @brief Implementacao da função que calcula a taxa de crescimento de municipios, 
         da função que carrega os alvos e gera o arquivo de saida e da funcao que cria o script para geracao do grafico
* @author Pedro Emerick (p.emerick@live.com)
* @since 09/04/17
* @date 13/04/17
*/

#include "funcoes_extra.h"

/**
* @brief Função que calcula a taxa de crescimento de cada municipio em determinados anos
* @param dados_municipios Dados de todos municipios
* @param taxas_municipios Codigo, nome e taxa de crescimento de todos municipios
* @param num_municipios Quantidade de municipios existentes
* @param arq_dados Determinado arquivo para fazer a sua leitura
*/
void taxa_crescimento_extra (Stats dados_municipios [], Taxa taxas_municipios [], int num_municipios, ifstream &arq_dados)
{
    string temp;    // String temporaria apenas para melhor leitura dos dados

    // Voltando para o inicio do arquivo
    arq_dados.clear();
    arq_dados.seekg(0, arq_dados.beg);

    getline (arq_dados, temp);

    // Leitura dos dados necessarios
    for (int ii = 0; ii < num_municipios; ii++)
    {
        getline (arq_dados, temp, '"');
        getline (arq_dados, taxas_municipios[ii].codigo, ' ');
        getline (arq_dados, taxas_municipios[ii].nome, '"');
        getline (arq_dados, temp);
    }

    // Calculo da taxa de crescimento
    for (int ii = 0; ii < num_municipios; ii++)
    {
        for (int jj = 1; jj < 21; jj++)
        {
            if (dados_municipios[ii].nascimentos[jj-1] != 0)
            {
                taxas_municipios[ii].taxa_crescimento[jj-1] = (float) dados_municipios[ii].nascimentos[jj] / (float) dados_municipios[ii].nascimentos[jj-1];
                taxas_municipios[ii].taxa_crescimento[jj-1] = 100 * (taxas_municipios[ii].taxa_crescimento[jj-1] - 1);
            }
            else
                taxas_municipios[ii].taxa_crescimento[jj-1] = 0;
        }
    }
}

/**
* @brief Função que faz a leitura dos alvos, gera o arquivo de saida para plotagem do grafico 
         e chama a funcao para criação do script
* @param taxas_municipios Codigo, nome e taxa de crescimento de todos municipios
* @param num_municipios Quantidade de municipios existentes
*/
void saida_alvos (Taxa taxas_municipios[], int num_municipios)
{
    string temp;    // String temporaria apenas para melhor leitura dos dados

    ifstream alvos ("./data/alvos.dat");
    ofstream extra ("./data/extra.dat");

    if (!alvos || !extra)
    {
        cerr << "O arquivo nao foi aberto !!!" << endl;

        exit (1);
    }

    int num_alvos = 0;
    string *codigos_alvos;      /**< String para armazenar o codigo dos alvos definidos */

    cerr << "... Lendo arquivo alvos.dat" << endl;

    num_alvos = conta_linhas (alvos);

    cerr << endl << "......" << "[" << num_alvos << "] municipios definidos como alvo" << endl;

    codigos_alvos = new string [num_alvos];

    // Voltando para o inicio do arquivo
    alvos.clear();
    alvos.seekg(0, alvos.beg);

    // Lendo alvos
    for (int ii = 0; ii < num_alvos; ii++)
    {
        getline (alvos, codigos_alvos[ii]);
    }

    int ano = 1994;

    string *nomes_alvos = new string [num_alvos];       /**< Vetor de strings que armazena apenas os nomes dos alvos */
    int ll = 0;

    int *indice_alvos = new int [num_alvos];        /**< Vetor de inteiros que armazena apenas o indice dos alvos */
    
    for (int jj = 0; jj < num_alvos; jj++)
    {
        for (int ii = 0; ii < num_municipios; ii++)
        {
            if (taxas_municipios[ii].codigo == codigos_alvos[jj])
            {
                nomes_alvos [ll] = taxas_municipios[ii].nome;
                indice_alvos [ll] = ii;                
                ll ++;

                cerr << "......{ " << taxas_municipios[ii].nome << " }" << endl;
            }
        }
    }
    
    // Grava no arquivo extra.dat
    for (int kk = -1; kk < 20; kk++)
    {
        extra << ano;

        for (int ii = 0; ii < num_alvos; ii ++)
        {
            if (kk == -1)
                extra << setw (10) << "0";
            else
                extra << setw (10) << taxas_municipios[indice_alvos[ii]].taxa_crescimento[kk];
        }

        extra << endl;

        ano++;
    }

    cerr << "... Arquivo extra.dat gerado" << endl; 

    cria_script (num_alvos, nomes_alvos);

    alvos.close ();
    extra.close ();

    delete [] nomes_alvos;
    delete [] indice_alvos;
    delete [] codigos_alvos;
}

/**
* @brief Funcao que faz a criação do script para geracao do grafico do arquivo extra.dat
* @param num_alvos Qauntidade de alvos definidos
* @param nomes_alvos Nome de cada alvo definido
*/
void cria_script (int num_alvos, string nomes_alvos [])
{
    ofstream script ("./scripts/extra.gnuplot");

    script << "clear" << endl;
    script << "reset" << endl;
    script << "set key off" << endl << endl;

    script << "set terminal png size 1024, 768 enhanced font 'Helvetica, 12'" << endl;
    script << "set output './images/extra.png'" << endl << endl;

    script << "set xlabel 'Ano'" << endl;
    script << "set ylabel 'Taxa de Crescimento'" << endl;
    script << "set key box right top inside" << endl << endl;

    script << "set title 'Taxa de Crescimento'" << endl << endl;

    script << "set xrange [1994:2014]" << endl;
    script << "set xtics 1" << endl << endl;

    int jj = 0;

    for (int ii = 2; ii <= num_alvos+1; ii++)
    {
        if (ii == 2)
            script << "plot './data/extra.dat' using 1:" << ii << " with lines linewidth 3 title '" << nomes_alvos[jj] << "'"<< ", ";
        else if (ii == num_alvos+1)
            script << "'./data/extra.dat' using 1:" << ii << " with lines linewidth 3 title '" << nomes_alvos[jj] << "'" << endl;
        else
            script << "'./data/extra.dat' using 1:" << ii << " with lines linewidth 3 title '" << nomes_alvos[jj] << "'" << ", ";
        
        jj++;
    }

    script.close ();
}

