# Inicializacao
clear
reset
set key off

# Exportacao para o formato .png
set terminal png size 640, 480 enhanced font 'Helvetica, 12'
set output './images/histograma.png'

# Titulo do grafico
set title 'Total de nascidos vivos no RN (1994-2014)'

# Configuracoes do eixo horizontal
set xrange [1994:2014]
set xtics 1
set xtic rotate by -45 scale 0

# Configuracoes do eixo vertical
set yrange [0:80000]

# Selecao do tipo de grafico a ser gerado (histograma)
set style data histogram
set style histogram clustered gap 1
set style fill solid border -1
set linetype 1 lc rgb 'red'
set boxwidth 0.6

# Plotagem do grafico
plot './data/totais.dat' using 1:2 title '' smooth freq with boxes


