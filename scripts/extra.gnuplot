clear
reset
set key off

set terminal png size 1024, 768 enhanced font 'Helvetica, 12'
set output './images/extra.png'

set xlabel 'Ano'
set ylabel 'Taxa de Crescimento'
set key box right top inside

set title 'Taxa de Crescimento'

set xrange [1994:2014]
set xtics 1

plot './data/extra.dat' using 1:2 with lines linewidth 3 title 'Natal', './data/extra.dat' using 1:3 with lines linewidth 3 title 'Parnamirim', './data/extra.dat' using 1:4 with lines linewidth 3 title 'Extremoz', './data/extra.dat' using 1:5 with lines linewidth 3 title 'S�o Gon�alo do Amarante', './data/extra.dat' using 1:6 with lines linewidth 3 title 'Cear�-Mirim'
